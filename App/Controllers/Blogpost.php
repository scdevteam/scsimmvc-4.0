<?php
namespace App\Controllers;
use App\Core\Controller;
use App\Core\View;
/**
 *  Blogpost
 */
class Blogpost extends Controller
{

	public function __construct()
	{
		parent::__construct();

		//Setting global css file
		self::setStyles('app');
	}

	public function indexAction( $args = array() )
	{
		$scVariables = self::getScVariables();
		$scVariables['scTitle'] = $args['title'];

		//input
		$input = array( 'id' => $args['id'], 'title' => $args['title'], 'name' => 'kribo');

		self::setStyles('sc');
		$scStyles = self::getStyles();

		$data = array_merge($scVariables, $scStyles, $input);

		View::render('f1', 'blogPost' , $data);



	}


	// public function jumpAction( $args = array() )
	// {
	// 	echo '<pre>';
	// 	var_dump($args);
	// 	echo '</pre>';


	// 	$scVariables = self::getScVariables();
	// 	$scVariables['scTitle'] = $args['title'];

	// 	//input
	// 	$input = array( 'id' => $args['id'], 'title' => $args['title']);

	// 	self::setStyles('sc');
	// 	$scStyles = self::getStyles();

	// 	$data = array_merge($scVariables, $scStyles, $input);

	// 	View::render('f1', 'blogPost' , $data);
	// }


	protected function before(){}

    protected function after(){}

}

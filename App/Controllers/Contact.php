<?php
namespace App\Controllers;
use App\Core\Controller;
use App\Core\View;
/**
 *  Contact
 */
class Contact extends Controller
{

	public function __construct()
	{
		parent::__construct();

		//Setting global css file
		self::setStyles('app');
	}

	public function indexAction()
	{
		View::render('f1', 'contact');
	}
	   
	protected function before(){}

    protected function after(){}

}
// END CLASS
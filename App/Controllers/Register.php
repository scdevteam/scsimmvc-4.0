<?php
namespace App\Controllers;
use App\Core\Controller;
use App\Core\View;
/**
 *  Register
 */
class Register extends Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function indexAction()
	{
			View::render('b0', 'register');
	}

	protected function before(){}

    protected function after(){}

} //END CLASS